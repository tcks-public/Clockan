﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clockan.Adjustment.TESTS
{
	[TestClass]
	public class FluentClockAdjustment_Tests {
		[TestMethod]
		public void WhenCallingAllFluentMethodsNothingCrashes() {
			AdjustClock.Adjust(x => {
				x.Default()
					.Freeze()
					.Measure()
					.MoveBy(TimeSpan.FromMinutes(-4))
					.MoveTo(new DateTimeOffset());
			})
				.Default()
				.Freeze()
				.Measure()
				.MoveBy(TimeSpan.FromMinutes(-4))
				.MoveTo(new DateTimeOffset())
				.Default(() => {
					AdjustClock.With.Default()
								.Freeze()
								.Measure()
								.MoveBy(TimeSpan.FromMinutes(-4))
								.MoveTo(new DateTimeOffset());
				})
				.Default(x => {
					x.Default()
						.Freeze()
						.Measure()
						.MoveBy(TimeSpan.FromMinutes(-4))
						.MoveTo(new DateTimeOffset());
				})
				.Freeze(() => {
					AdjustClock.With.Default()
									.Freeze()
									.Measure()
									.MoveBy(TimeSpan.FromMinutes(-4))
									.MoveTo(new DateTimeOffset());
				})
				.Freeze(x => {
					x.Default()
						.Freeze()
						.Measure()
						.MoveBy(TimeSpan.FromMinutes(-4))
						.MoveTo(new DateTimeOffset());
				})
				.Measure(() => {
					AdjustClock.With.Default()
									.Freeze()
									.Measure()
									.MoveBy(TimeSpan.FromMinutes(-4))
									.MoveTo(new DateTimeOffset());
				})
				.Measure(x => {
					x.Default()
						.Freeze()
						.Measure()
						.MoveBy(TimeSpan.FromMinutes(-4))
						.MoveTo(new DateTimeOffset());
				})
				.MoveBy(TimeSpan.FromMinutes(-7), () => {
					AdjustClock.With.Default()
									.Freeze()
									.Measure()
									.MoveBy(TimeSpan.FromMinutes(-4))
									.MoveTo(new DateTimeOffset());
				})
				.MoveBy(TimeSpan.FromMinutes(-8), x => {
					x.Default()
						.Freeze()
						.Measure()
						.MoveBy(TimeSpan.FromMinutes(-4))
						.MoveTo(new DateTimeOffset());
				})
				.MoveTo(new DateTimeOffset(), () => {
					AdjustClock.With.Default()
								.Freeze()
								.Measure()
								.MoveBy(TimeSpan.FromMinutes(-4))
								.MoveTo(new DateTimeOffset());
				})
				.MoveTo(new DateTimeOffset(), x => {
					x.Default()
						.Freeze()
						.Measure()
						.MoveBy(TimeSpan.FromMinutes(-4))
						.MoveTo(new DateTimeOffset());
				})
				.Default(a => {
					a.Freeze(b => {
						b.Measure(c => {
							c.MoveBy(TimeSpan.FromMinutes(-15), d => {
								d.MoveTo(new DateTimeOffset(), e => {
									AdjustClock.With.Default(() => {
										AdjustClock.With.Measure(() => {
											AdjustClock.With.MoveBy(TimeSpan.FromMinutes(-20), () => {
												AdjustClock.With.MoveTo(new DateTimeOffset(), () => {
													// do nothing
													return;
												});
											});
										});
									});
								});
							});
						});
					});
				});

			// nothing crashes => ok
			Assert.IsTrue(true);
		}
	}
}
