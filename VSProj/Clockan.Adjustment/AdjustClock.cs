﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clockan.Adjustment
{
	/// <summary>
	/// Contains static members helping to adjust <see cref="Clock"/>.
	/// </summary>
	public static class AdjustClock {
		/// <summary>
		/// Entry point to fluent interface for <see cref="Clock"/> adjustment.
		/// </summary>
		/// <returns>
		/// Returns instance of <see cref="FluentClockAdjustment"/>.
		/// </returns>
		public static FluentClockAdjustment With => default(FluentClockAdjustment);

		public static FluentClockAdjustment Adjust(Action<FluentClockAdjustment> action) {
			var result = With;
			action(result);
			return result;
		}
	}
}
