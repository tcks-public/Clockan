﻿using System;
using System.Collections.Generic;
using System.Text;
using Clockan.Config;

namespace Clockan.Adjustment
{
	/// <summary>
	/// Provides adjustment of <see cref="Clock"/> with fluent interface.
	/// </summary>
	public struct FluentClockAdjustment {
		#region Preserve
		/// <summary>
		/// Preserves current configuration of <see cref="Clock"/>, invokes <paramref name="action"/> and returns preserved configuration of <see cref="Clock"/>.
		/// </summary>
		/// <param name="action">Action to run.</param>
		/// <returns>
		/// Returns self (this).
		/// </returns>
		public FluentClockAdjustment Preserve(Action action) {
			var origData = ClockConfig.GetData();
			try {
				action();
			}
			finally {
				ClockConfig.SetData(origData);
			}

			return this;
		}

		/// <summary>
		/// Preserves current configuration of <see cref="Clock"/>, invokes <paramref name="action"/> and returns preserved configuration of <see cref="Clock"/>.
		/// </summary>
		/// <param name="action">Action to run.</param>
		/// <returns>
		/// Returns self (this).
		/// </returns>
		public FluentClockAdjustment Preserve(Action<FluentClockAdjustment> action) {
			var origData = ClockConfig.GetData();
			try {
				action(this);
			}
			finally {
				ClockConfig.SetData(origData);
			}

			return this;
		}

		private FluentClockAdjustment Preserve(Func<FluentClockAdjustment> action1, Action action2) {
			var origData = ClockConfig.GetData();
			try {
				action1();
				action2();
			}
			finally {
				ClockConfig.SetData(origData);
			}

			return this;
		}

		private FluentClockAdjustment Preserve(Func<FluentClockAdjustment> action1, Action<FluentClockAdjustment> action2) {
			var origData = ClockConfig.GetData();
			try {
				action1();
				action2(this);
			}
			finally {
				ClockConfig.SetData(origData);
			}

			return this;
		}

		private FluentClockAdjustment Preserve<T1Arg0>(Func<T1Arg0, FluentClockAdjustment> action1, T1Arg0 t1arg0, Action action2) {
			var origData = ClockConfig.GetData();
			try {
				action1(t1arg0);
				action2();
			}
			finally {
				ClockConfig.SetData(origData);
			}

			return this;
		}

		private FluentClockAdjustment Preserve<T1Arg0>(Func<T1Arg0, FluentClockAdjustment> action1, T1Arg0 t1arg0, Action<FluentClockAdjustment> action2) {
			var origData = ClockConfig.GetData();
			try {
				action1(t1arg0);
				action2(this);
			}
			finally {
				ClockConfig.SetData(origData);
			}

			return this;
		}
		#endregion Preserve

		#region Freeze
		/// <summary>
		/// Sets configuration of <see cref="Clock"/> to constant time currently returned from <see cref="Clock.Utc.Now"/>.
		/// </summary>
		/// <returns>
		/// Returns self (this).
		/// </returns>
		public FluentClockAdjustment Freeze() => Freeze(Clock.Utc.Now);

		/// <summary>
		/// Sets configuration of <see cref="Clock"/> to constant time passed in <paramref name="currentTime"/>.
		/// </summary>
		/// <param name="currentTime">The time used in configuration of <see cref="Clock"/>.</param>
		/// <returns>
		/// Returns self (this).
		/// </returns>
		public FluentClockAdjustment Freeze(DateTimeOffset currentTime) {
			var utc = currentTime.ToUniversalTime();
			var local = currentTime.ToLocalTime();
			var newData = ClockConfigData.Create(() => utc, () => local);

			ClockConfig.SetData(newData);

			return this;
		}

		/// <summary>
		/// Preserves current configuration of <see cref="Clock"/>, freeze current time via <see cref="Freeze"/>, invokes <paramref name="action"/> and returns preserved configuration of <see cref="Clock"/>.
		/// </summary>
		/// <param name="action"></param>
		/// <returns></returns>
		public FluentClockAdjustment Freeze(Action action) => Freeze(Clock.Utc.Now, action);

		public FluentClockAdjustment Freeze(DateTimeOffset currentTime, Action action) => Preserve(Freeze, currentTime, action);

		/// <summary>
		/// Preserves current configuration of <see cref="Clock"/>, freeze current time via <see cref="Freeze"/>, invokes <paramref name="action"/> and returns preserved configuration of <see cref="Clock"/>.
		/// </summary>
		/// <param name="action"></param>
		/// <returns></returns>
		public FluentClockAdjustment Freeze(Action<FluentClockAdjustment> action) => Freeze(Clock.Utc.Now, action);

		public FluentClockAdjustment Freeze(DateTimeOffset currentTime, Action<FluentClockAdjustment> action) => Preserve(Freeze, currentTime, action);
		#endregion Freeze

		#region MoveBy
		public FluentClockAdjustment MoveBy(TimeSpan time) {
			var origData = ClockConfig.GetData();
			var origUtcNow = origData?.UtcNow ?? Clock.Default.GetUtcNow;
			var origLocalNow = origData?.LocalNow ?? Clock.Default.GetLocalNow;
			var newData = ClockConfigData.Create(
				utcNow: () => origUtcNow() + time,
				localNow: () => origLocalNow() + time
			);

			ClockConfig.SetData(newData);

			return this;
		}

		public FluentClockAdjustment MoveBy(TimeSpan time, Action action) => Preserve(MoveBy, time, action);

		public FluentClockAdjustment MoveBy(TimeSpan time, Action<FluentClockAdjustment> action) => Preserve(MoveBy, time, action);
		#endregion MoveBy

		#region MoveTo
		public FluentClockAdjustment MoveTo(DateTimeOffset currentTime) {
			var currentUtc = currentTime.ToUniversalTime();
			var diff = Clock.Utc.Now - currentUtc;

			return MoveBy(diff);
		}

		public FluentClockAdjustment MoveTo(DateTimeOffset currentTime, Action action) => Preserve(MoveTo, currentTime, action);

		public FluentClockAdjustment MoveTo(DateTimeOffset currentTime, Action<FluentClockAdjustment> action) => Preserve(MoveTo, currentTime, action);
		#endregion MoveTo

		#region Default
		public FluentClockAdjustment Default() {
			ClockConfig.ResetData();

			return this;
		}

		public FluentClockAdjustment Default(Action action) => Preserve(Default, action);

		public FluentClockAdjustment Default(Action<FluentClockAdjustment> action) => Preserve(Default, action);
		#endregion Default

		#region Measure
		public FluentClockAdjustment Measure() {
			var utcNow = Clock.Utc.Now;
			var swatch = System.Diagnostics.Stopwatch.StartNew();

			var utcStart = utcNow.ToUniversalTime();
			var localStart = utcNow.ToLocalTime();
			var newData = ClockConfigData.Create(
				utcNow: () => utcStart.AddTicks(swatch.ElapsedTicks),
				localNow: () => localStart.AddTicks(swatch.ElapsedTicks)
			);

			ClockConfig.SetData(newData);

			return this;
		}

		public FluentClockAdjustment Measure(Action action) => Preserve(Measure, action);

		public FluentClockAdjustment Measure(Action<FluentClockAdjustment> action) => Preserve(Measure, action);
		#endregion Measure
	}
}
