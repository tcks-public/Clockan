﻿using System;
using System.Collections.Generic;
using System.Text;
using Clockan.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clockan.TESTS {
	[TestClass]
	public class ClockConfigData_Tests {
		[TestMethod]
		public void EmptyConstructorWorks() {
			var data = new ClockConfigData();
			Assert.IsNull(data.UtcNow);
			Assert.IsNull(data.LocalNow);
		}

		[TestMethod]
		public void ConstructorWorks() {
			Func<DateTimeOffset> utc = () => new DateTimeOffset();
			Func<DateTimeOffset> local = () => new DateTimeOffset();

			{
				var data = new ClockConfigData(null, null);
				Assert.IsNull(data.UtcNow);
				Assert.IsNull(data.LocalNow);
			}

			{
				var data = new ClockConfigData(utc, null);
				Assert.AreSame(utc, data.UtcNow);
				Assert.IsNull(data.LocalNow);
			}

			{
				var data = new ClockConfigData(null, local);
				Assert.IsNull(data.UtcNow);
				Assert.AreSame(local, data.LocalNow);
			}

			{
				var data = new ClockConfigData(utc, local);
				Assert.AreSame(utc, data.UtcNow);
				Assert.AreSame(local, data.LocalNow);
			}
		}

		[TestMethod]
		public void CreateWorks() {
			Func<DateTimeOffset> utc = () => new DateTimeOffset();
			Func<DateTimeOffset> local = () => new DateTimeOffset();

			{
				var data = ClockConfigData.Create(null, null);
				Assert.IsNull(data.UtcNow);
				Assert.IsNull(data.LocalNow);
			}

			{
				var data = ClockConfigData.Create(utc, null);
				Assert.AreSame(utc, data.UtcNow);
				Assert.IsNull(data.LocalNow);
			}

			{
				var data = ClockConfigData.Create(null, local);
				Assert.IsNull(data.UtcNow);
				Assert.AreSame(local, data.LocalNow);
			}

			{
				var data = ClockConfigData.Create(utc, local);
				Assert.AreSame(utc, data.UtcNow);
				Assert.AreSame(local, data.LocalNow);
			}
		}

		[TestMethod]
		public void CreateFromOriginalWorks() {
			Func<DateTimeOffset> utc = () => new DateTimeOffset();
			Func<DateTimeOffset> local = () => new DateTimeOffset();

			#region NULL
			{
				var data = ClockConfigData.Create(null, null, null);
				Assert.IsNull(data.UtcNow);
				Assert.IsNull(data.LocalNow);
			}

			{
				var data = ClockConfigData.Create(null, utc, null);
				Assert.AreSame(utc, data.UtcNow);
				Assert.IsNull(data.LocalNow);
			}

			{
				var data = ClockConfigData.Create(null, null, local);
				Assert.IsNull(data.UtcNow);
				Assert.AreSame(local, data.LocalNow);
			}

			{
				var data = ClockConfigData.Create(null, utc, local);
				Assert.AreSame(utc, data.UtcNow);
				Assert.AreSame(local, data.LocalNow);
			}
			#endregion NULL

			#region NULL, NULL
			{
				var origNullNull = new ClockConfigData(null, null);

				Assert.AreNotSame(utc, origNullNull.UtcNow);
				Assert.AreNotSame(local, origNullNull.LocalNow);

				{
					var data = ClockConfigData.Create(origNullNull, null, null);
					Assert.IsNull(data.UtcNow);
					Assert.IsNull(data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origNullNull, utc, null);
					Assert.AreSame(utc, data.UtcNow);
					Assert.IsNull(data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origNullNull, null, local);
					Assert.IsNull(data.UtcNow);
					Assert.AreSame(local, data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origNullNull, utc, local);
					Assert.AreSame(utc, data.UtcNow);
					Assert.AreSame(local, data.LocalNow);
				}
			}
			#endregion NULL, NULL

			#region Utc, NULL
			{
				var origUtcNull = new ClockConfigData(() => new DateTimeOffset(), null);

				Assert.AreNotSame(utc, origUtcNull.UtcNow);
				Assert.AreNotSame(local, origUtcNull.LocalNow);

				{
					var data = ClockConfigData.Create(origUtcNull, null, null);
					Assert.AreSame(origUtcNull.UtcNow, data.UtcNow);
					Assert.IsNull(data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origUtcNull, utc, null);
					Assert.AreSame(utc, data.UtcNow);
					Assert.IsNull(data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origUtcNull, null, local);
					Assert.AreSame(origUtcNull.UtcNow, data.UtcNow);
					Assert.AreSame(local, data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origUtcNull, utc, local);
					Assert.AreSame(utc, data.UtcNow);
					Assert.AreSame(local, data.LocalNow);
				}
			}
			#endregion Utc, NULL

			#region NULL, Local
			{
				var origNullLocal = new ClockConfigData(null, () => new DateTimeOffset());

				Assert.AreNotSame(utc, origNullLocal.UtcNow);
				Assert.AreNotSame(local, origNullLocal.LocalNow);

				{
					var data = ClockConfigData.Create(origNullLocal, null, null);
					Assert.AreSame(origNullLocal.UtcNow, data.UtcNow);
					Assert.AreSame(origNullLocal.LocalNow, data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origNullLocal, utc, null);
					Assert.AreSame(utc, data.UtcNow);
					Assert.AreSame(origNullLocal.LocalNow, data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origNullLocal, null, local);
					Assert.AreSame(null, data.UtcNow);
					Assert.AreSame(local, data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origNullLocal, utc, local);
					Assert.AreSame(utc, data.UtcNow);
					Assert.AreSame(local, data.LocalNow);
				}
			}
			#endregion NULL, Local

			#region Utc, Local
			{
				var origUtcLocal = new ClockConfigData(() => new DateTimeOffset(), () => new DateTimeOffset());

				Assert.AreNotSame(utc, origUtcLocal.UtcNow);
				Assert.AreNotSame(local, origUtcLocal.LocalNow);

				{
					var data = ClockConfigData.Create(origUtcLocal, null, null);
					Assert.AreSame(origUtcLocal.UtcNow, data.UtcNow);
					Assert.AreSame(origUtcLocal.LocalNow, data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origUtcLocal, utc, null);
					Assert.AreSame(utc, data.UtcNow);
					Assert.AreSame(origUtcLocal.LocalNow, data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origUtcLocal, null, local);
					Assert.AreSame(origUtcLocal.UtcNow, data.UtcNow);
					Assert.AreSame(local, data.LocalNow);
				}

				{
					var data = ClockConfigData.Create(origUtcLocal, utc, local);
					Assert.AreSame(utc, data.UtcNow);
					Assert.AreSame(local, data.LocalNow);
				}
			}
			#endregion Utc, Local
		}

		[TestMethod]
		public void WithUtcNowAndWithLocalNowWorks() {
			Func<DateTimeOffset> utc1 = () => new DateTimeOffset();
			Func<DateTimeOffset> utc2 = () => new DateTimeOffset();

			Func<DateTimeOffset> local1 = () => new DateTimeOffset();
			Func<DateTimeOffset> local2 = () => new DateTimeOffset();

			var data0 = ClockConfigData.WithUtcNow(null, null);
			Assert.IsNotNull(data0);
			Assert.IsNull(data0.UtcNow);
			Assert.IsNull(data0.LocalNow);

			{
				var data1 = ClockConfigData.WithUtcNow(data0, null);
				Assert.IsNotNull(data1);
				Assert.AreNotSame(data0, data1);
				Assert.IsNull(data1.UtcNow);
				Assert.IsNull(data1.LocalNow);
			}

			{
				var data1 = ClockConfigData.WithLocalNow(data0, null);
				Assert.IsNotNull(data1);
				Assert.AreNotSame(data0, data1);
				Assert.IsNull(data1.UtcNow);
				Assert.IsNull(data1.LocalNow);
			}

			{
				var data1 = ClockConfigData.WithUtcNow(data0, utc1);
				Assert.IsNotNull(data1);
				Assert.AreNotSame(data0, data1);
				Assert.AreSame(utc1, data1.UtcNow);
				Assert.IsNull(data1.LocalNow);
			}

			{
				var data1 = ClockConfigData.WithLocalNow(data0, local1);
				Assert.IsNotNull(data1);
				Assert.AreNotSame(data0, data1);
				Assert.IsNull(data1.UtcNow);
				Assert.AreSame(local1, data1.LocalNow);
			}

			var dataX = ClockConfigData.WithUtcNow(data0, utc1);
			Assert.AreSame(utc1, dataX.UtcNow);
			Assert.IsNull(dataX.LocalNow);

			dataX = ClockConfigData.WithLocalNow(dataX, local1);
			Assert.AreSame(utc1, dataX.UtcNow);
			Assert.AreSame(local1, dataX.LocalNow);

			dataX = ClockConfigData.WithUtcNow(dataX, utc2);
			Assert.AreSame(utc2, dataX.UtcNow);
			Assert.AreSame(local1, dataX.LocalNow);

			dataX = ClockConfigData.WithLocalNow(dataX, local2);
			Assert.AreSame(utc2, dataX.UtcNow);
			Assert.AreSame(local2, dataX.LocalNow);

			dataX = ClockConfigData.WithUtcNow(dataX, null);
			Assert.AreSame(null, dataX.UtcNow);
			Assert.AreSame(local2, dataX.LocalNow);

			dataX = ClockConfigData.WithLocalNow(dataX, null);
			Assert.AreSame(null, dataX.UtcNow);
			Assert.AreSame(null, dataX.LocalNow);
		}

		[TestMethod]
		public void CloneWorks() {
			#region NULL, NULL
			{
				var original = new ClockConfigData();
				var clone = original.Clone();
				Assert.AreNotSame(original, clone);
				Assert.AreSame(original.UtcNow, clone.UtcNow);
				Assert.AreSame(original.LocalNow, clone.LocalNow);
			}
			#endregion NULL, NULL

			#region Utc, NULL
			{
				var original = new ClockConfigData(() => new DateTimeOffset(), null);
				var clone = original.Clone();
				Assert.AreNotSame(original, clone);
				Assert.AreSame(original.UtcNow, clone.UtcNow);
				Assert.AreSame(original.LocalNow, clone.LocalNow);
			}
			#endregion Utc, NULL

			#region NULL, Local
			{
				var original = new ClockConfigData(null, () => new DateTimeOffset());
				var clone = original.Clone();
				Assert.AreNotSame(original, clone);
				Assert.AreSame(original.UtcNow, clone.UtcNow);
				Assert.AreSame(original.LocalNow, clone.LocalNow);
			}
			#endregion NULL, Local

			#region Utc, Local
			{
				var original = new ClockConfigData(() => new DateTimeOffset(), () => new DateTimeOffset());
				var clone = original.Clone();
				Assert.AreNotSame(original, clone);
				Assert.AreSame(original.UtcNow, clone.UtcNow);
				Assert.AreSame(original.LocalNow, clone.LocalNow);
			}
			#endregion Utc, Local
		}

		[TestMethod]
		public void CanLockAndUnlock() {
			Assert.IsFalse(ClockConfig.IsLocked());

			var rsltLock0 = ClockConfig.TryLock(out var lck0);
			Assert.IsTrue(rsltLock0);
			Assert.IsNotNull(lck0);
			Assert.IsTrue(ClockConfig.IsLocked());

			var rsltUnlock0 = ClockConfig.TryUnlock(new object());
			Assert.IsFalse(rsltUnlock0);

			var rsltUnlock1 = ClockConfig.TryUnlock(lck0);
			Assert.IsTrue(rsltUnlock1);
			Assert.IsFalse(ClockConfig.IsLocked());
		}

		[TestMethod]
		public void ExceptionIsThrownWhenLockedConfigIsEdited() {
			Func<DateTimeOffset> utcNow = () => new DateTimeOffset();

			Assert.IsFalse(ClockConfig.IsLocked());
			Assert.IsNull(ClockConfig.GetData()?.UtcNow);

			var lck0 = ClockConfig.Lock();
			Assert.ThrowsException<ConfigIsLockedException>(() => ClockConfig.SetData(new ClockConfigData(utcNow, null)));
			Assert.IsNull(ClockConfig.GetData()?.UtcNow);
		}
	}
}
