﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Clockan.Config;

namespace Clockan.TESTS
{
	[TestClass]
	public class ClockConfig_Tests {
		[TestInitialize]
		public void PrepareTest() {
			ClockConfig.ResetData();
		}

		[TestCleanup]
		public void CleanTest() {
			ClockConfig.ResetData();
		}

		[TestMethod]
		public void SetDataAndGetDataAndResetWorks() {
			ClockConfig.SetData(null);
			Assert.IsNull(ClockConfig.GetData());
			Assert.IsNull(ClockConfig.UtcNow);
			Assert.IsNull(ClockConfig.LocalNow);

			{
				ClockConfig.SetData(null, out var prevData);
				Assert.IsNull(prevData);
			}

			Func<DateTimeOffset> utc = () => new DateTimeOffset();
			Func<DateTimeOffset> local = () => new DateTimeOffset();

			var data0 = ClockConfigData.Create(utc, local);
			{
				{
					ClockConfig.SetData(data0, out var prevData);
					Assert.IsNull(prevData);
					Assert.AreSame(utc, ClockConfig.UtcNow);
					Assert.AreSame(local, ClockConfig.LocalNow);
				}

				var currData0 = ClockConfig.GetData();
				Assert.IsNotNull(currData0);
				Assert.AreNotSame(data0, currData0);
				Assert.AreSame(data0.UtcNow, currData0.UtcNow);
				Assert.AreSame(data0.LocalNow, currData0.LocalNow);
				Assert.AreSame(data0.UtcNow, ClockConfig.UtcNow);
				Assert.AreSame(data0.LocalNow, ClockConfig.LocalNow);

				var currData1 = ClockConfig.GetData();
				Assert.IsNotNull(currData1);
				Assert.AreNotSame(data0, currData1);
				Assert.AreNotSame(currData0, currData1);
				Assert.AreSame(data0.UtcNow, currData1.UtcNow);
				Assert.AreSame(data0.LocalNow, currData1.LocalNow);

				Func<DateTimeOffset> utc2 = () => new DateTimeOffset();
				Func<DateTimeOffset> local2 = () => new DateTimeOffset();

				var nextData0 = new ClockConfigData(utc2, local2);
				{
					ClockConfig.SetData(nextData0, out var prevData);
					Assert.IsNotNull(prevData);
					Assert.AreNotSame(currData1, prevData);
					Assert.AreSame(currData1.UtcNow, prevData.UtcNow);
					Assert.AreSame(currData1.LocalNow, prevData.LocalNow);
					Assert.AreSame(nextData0.UtcNow, ClockConfig.UtcNow);
					Assert.AreSame(nextData0.LocalNow, ClockConfig.LocalNow);
				}

				var currData2 = ClockConfig.GetData();
				Assert.IsNotNull(currData2);
				Assert.AreNotSame(nextData0, currData2);
				Assert.AreSame(nextData0.UtcNow, currData2.UtcNow);
				Assert.AreSame(nextData0.LocalNow, currData2.LocalNow);
				Assert.AreSame(nextData0.UtcNow, ClockConfig.UtcNow);
				Assert.AreSame(nextData0.LocalNow, ClockConfig.LocalNow);

				ClockConfig.ResetData(out var prevResetData);
				Assert.AreNotSame(nextData0, prevResetData);
				Assert.AreSame(nextData0.UtcNow, prevResetData.UtcNow);
				Assert.AreSame(nextData0.LocalNow, prevResetData.LocalNow);

				Assert.IsNull(ClockConfig.GetData());
				Assert.IsNull(ClockConfig.GetData());
			}
		}
	}
}
