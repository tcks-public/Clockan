﻿using System;
using System.Collections.Generic;
using System.Text;
using Clockan.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clockan.TESTS
{
	[TestClass]
	public class Clock_Tests {
		[TestInitialize]
		public void PrepareTest() {
			ClockConfig.ResetData();
		}

		[TestCleanup]
		public void CleanTest() {
			ClockConfig.ResetData();
		}

		[TestMethod]
		public void DefaultClockWorks() {
			var utc0 = Clock.Utc.Now;
			var utc1 = Clock.Utc.GetNow();

			var local0 = Clock.Local.Now;
			var local1 = Clock.Local.GetNow();

			var defaultUtc0 = Clock.Default.UtcNow;
			var defaultUtc1 = Clock.Default.GetUtcNow();

			var defaultLocal0 = Clock.Default.LocalNow;
			var defaultLocal1 = Clock.Default.GetLocalNow();

			void AssertEqualTime(DateTimeOffset a, DateTimeOffset b) {
				var tolerance = TimeSpan.FromMilliseconds(1).Ticks;
				var diff = Math.Abs((a - b).Ticks);

				Assert.IsTrue(diff <= tolerance);
			}

			AssertEqualTime(Clock.Default.UtcNow, Clock.Utc.Now);
			AssertEqualTime(Clock.Default.UtcNow, Clock.Utc.GetNow());

			AssertEqualTime(Clock.Default.GetUtcNow(), Clock.Utc.Now);
			AssertEqualTime(Clock.Default.GetUtcNow(), Clock.Utc.GetNow());

			AssertEqualTime(Clock.Default.LocalNow, Clock.Local.Now);
			AssertEqualTime(Clock.Default.LocalNow, Clock.Local.GetNow());

			AssertEqualTime(Clock.Default.GetLocalNow(), Clock.Local.Now);
			AssertEqualTime(Clock.Default.GetLocalNow(), Clock.Local.GetNow());
		}

		[TestMethod]
		public void FullyConfiguredClockWorks() {
			var utc = new DateTimeOffset(1234, 1, 2, 3, 4, 5, 6, TimeSpan.Zero);
			var local = new DateTimeOffset(1234, 1, 2, 3, 4, 5, 6, TimeSpan.FromHours(1.5d));

			ClockConfig.SetUtcNow(() => utc);
			ClockConfig.SetLocalNow(() => local);

			Assert.AreEqual(utc, Clock.Utc.Now);
			Assert.AreEqual(utc, Clock.Utc.GetNow());

			Assert.AreEqual(local, Clock.Local.Now);
			Assert.AreEqual(local, Clock.Local.GetNow());

			Assert.AreNotEqual(utc, Clock.Default.UtcNow);
			Assert.AreNotEqual(utc, Clock.Default.GetUtcNow());

			Assert.AreNotEqual(local, Clock.Default.LocalNow);
			Assert.AreNotEqual(local, Clock.Default.GetLocalNow());
		}

		[TestMethod]
		public void UtcConfiguredClockWorks() {
			var utc = new DateTimeOffset(1234, 1, 12, 3, 4, 5, 6, TimeSpan.Zero);

			ClockConfig.SetUtcNow(() => utc);

			Assert.AreEqual(utc, Clock.Utc.Now);
			Assert.AreEqual(utc, Clock.Utc.GetNow());

			Assert.AreEqual(Clock.Local.Now, Clock.Local.GetNow());

			Assert.AreEqual(utc.Year, Clock.Local.Now.Year);
			Assert.AreEqual(utc.Year, Clock.Local.GetNow().Year);

			Assert.AreNotEqual(Clock.Default.UtcNow, Clock.Utc.Now);
			Assert.AreNotEqual(Clock.Default.GetUtcNow(), Clock.Utc.Now);

			Assert.AreNotEqual(Clock.Default.UtcNow, Clock.Local.Now);
			Assert.AreNotEqual(Clock.Default.GetUtcNow(), Clock.Local.Now);
		}

		[TestMethod]
		public void LocalConfiguredClockWorks() {
			var local = new DateTimeOffset(1234, 1, 12, 3, 4, 5, 6, TimeSpan.FromHours(1.5d));

			ClockConfig.SetLocalNow(() => local);

			Assert.AreEqual(local, Clock.Local.Now);
			Assert.AreEqual(local, Clock.Local.GetNow());

			Assert.AreEqual(Clock.Utc.Now, Clock.Utc.GetNow());

			Assert.AreEqual(local.Year, Clock.Utc.Now.Year);
			Assert.AreEqual(local.Year, Clock.Utc.GetNow().Year);

			Assert.AreNotEqual(Clock.Default.UtcNow, Clock.Utc.Now);
			Assert.AreNotEqual(Clock.Default.GetUtcNow(), Clock.Utc.Now);

			Assert.AreNotEqual(Clock.Default.UtcNow, Clock.Local.Now);
			Assert.AreNotEqual(Clock.Default.GetUtcNow(), Clock.Local.Now);
		}
	}
}
