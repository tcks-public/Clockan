﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Clockan.Config;

namespace Clockan {
	/// <summary>
	/// Globaly accessible clock.
	/// </summary>
	public static class Clock {
		/// <summary>
		/// Contains static members related to UTC.
		/// </summary>
		public static class Utc {
			/// <summary>
			/// Returns current time in UTC.
			/// </summary>
			/// <remarks>
			/// If specified, returns result of <see cref="ClockConfig.UtcNow"/>.
			/// If specified, returns result of <see cref="ClockConfig.LocalNow"/> transformed to universal time via <see cref="DateTimeOffset.ToUniversalTime"/>.
			/// Otherwise returns result of <see cref="DateTimeOffset.UtcNow"/>.
			/// </remarks>
			public static DateTimeOffset Now => GetNow();

			/// <summary>
			/// Returns current time in UTC.
			/// </summary>
			/// <remarks>
			/// If specified, returns result of <see cref="ClockConfig.UtcNow"/>.
			/// If specified, returns result of <see cref="ClockConfig.LocalNow"/> transformed to universal time via <see cref="DateTimeOffset.ToUniversalTime"/>.
			/// Otherwise returns result of <see cref="DateTimeOffset.UtcNow"/>.
			/// </remarks>
			public static DateTimeOffset GetNow() => ClockConfig.UtcNow?.Invoke() ?? ClockConfig.LocalNow?.Invoke().ToUniversalTime() ?? Default.GetUtcNow();
		}

		/// <summary>
		/// Contains static members related to local time.
		/// </summary>
		public static class Local {
			/// <summary>
			/// Returns current time in current time zone.
			/// </summary>
			/// <remarks>
			/// If specified, returns result of <see cref="ClockConfig.LocalNow"/>.
			/// If specified, returns result of <see cref="ClockConfig.UtcNow"/> transformed to local time via <see cref="DateTimeOffset.ToLocalTime"/>.
			/// Otherwise returns result of <see cref="DateTimeOffset.Now"/>.
			/// </remarks>
			public static DateTimeOffset Now => GetNow();

			/// <summary>
			/// Returns current time in current time zone.
			/// </summary>
			/// <remarks>
			/// If specified, returns result of <see cref="ClockConfig.LocalNow"/>.
			/// If specified, returns result of <see cref="ClockConfig.UtcNow"/> transformed to local time via <see cref="DateTimeOffset.ToLocalTime"/>.
			/// Otherwise returns result of <see cref="DateTimeOffset.Now"/>.
			/// </remarks>
			public static DateTimeOffset GetNow() => ClockConfig.LocalNow?.Invoke() ?? ClockConfig.UtcNow?.Invoke().ToLocalTime() ?? Default.GetLocalNow();
		}

		/// <summary>
		/// Contains static members related to default behaviour.
		/// </summary>
		public static class Default {
			/// <summary>
			/// Returns current UTC via <see cref="DateTimeOffset.UtcNow"/>.
			/// </summary>
			public static DateTimeOffset UtcNow => GetUtcNow();

			/// <summary>
			/// Returns current UTC via <see cref="DateTimeOffset.UtcNow"/>.
			/// </summary>
			public static DateTimeOffset GetUtcNow() => DateTimeOffset.UtcNow;

			/// <summary>
			/// Returns current local time via <see cref="DateTimeOffset.Now"/>.
			/// </summary>
			public static DateTimeOffset LocalNow => GetLocalNow();

			/// <summary>
			/// Returns current local time via <see cref="DateTimeOffset.Now"/>.
			/// </summary>
			public static DateTimeOffset GetLocalNow() => DateTimeOffset.Now;
		}
	}
}
