﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Clockan.Config
{
	/// <summary>
	/// Contains configuration for <see cref="Clock"/>.
	/// </summary>
	public static class ClockConfig {
		private const int DefaultMaxAttempts = 1000;
		private static ClockConfigData data;
		private static object configAccessKey;

		private static void ThrowIfIsLocked() {
			if (IsLocked()) {
				throw new ConfigIsLockedException();
			}
		}

		public static bool IsLocked() => !(configAccessKey is null);
		public static bool TryLock(out object accessKey) {
			var tmpKey = new object();
			var currKey = Interlocked.CompareExchange(ref configAccessKey, tmpKey, null);
			if (currKey is null) {
				accessKey = tmpKey;
				return true;
			}

			accessKey = null;
			return false;
		}
		public static bool TryUnlock(object accessKey) {
			var currKey = Interlocked.CompareExchange(ref configAccessKey, null, accessKey);
			if (object.ReferenceEquals(currKey, accessKey)) {
				return true;
			}

			return false;
		}

		public static object Lock() {
			if (TryLock(out var result)) {
				return result;
			}

			throw new ConfigIsLockedException();
		}
		public static void Unlock(object accessKey) {
			if (TryUnlock(accessKey)) {
				return;
			}

			throw new ConfigIsLockedException();
		}

		private static void EditDataInternal(Func<DateTimeOffset> parameter, Func<ClockConfigData, Func<DateTimeOffset>, ClockConfigData> dataFactory, out ClockConfigData prevData) {
			EditDataInternal(DefaultMaxAttempts, parameter, dataFactory, out prevData);
		}
		private static void EditDataInternal(int maxAttempts, Func<DateTimeOffset> parameter, Func<ClockConfigData, Func<DateTimeOffset>, ClockConfigData> dataFactory, out ClockConfigData prevData) {
			if (TryEditDataInternal(maxAttempts, parameter, dataFactory, out prevData)) {
				return;
			}

			throw new InvalidOperationException("Race-condition error. Can not edit configuration data because something else is constantly changing internal data.");
		}
		private static bool TryEditDataInternal(int maxAttempts, Func<DateTimeOffset> parameter, Func<ClockConfigData, Func<DateTimeOffset>, ClockConfigData> dataFactory, out ClockConfigData prevData) {
			for (var i = 0; i < maxAttempts; i++) {
				ThrowIfIsLocked();

				var origData = data;
				var newData = dataFactory(origData, parameter);
				var tmpPrevData = Interlocked.CompareExchange(ref data, newData, origData);
				if (ReferenceEquals(origData, tmpPrevData)) {
					prevData = tmpPrevData?.Clone();
					return true;
				}
			}

			prevData = null;
			return false;
		}

		/// <summary>
		/// Explicit implementation of current time provider in UTC.
		/// </summary>
		public static Func<DateTimeOffset> UtcNow => data?.UtcNow;

		/// <summary>
		/// Explicit implementation of current time in local time.
		/// </summary>
		public static Func<DateTimeOffset> LocalNow => data?.LocalNow;

		public static ClockConfigData GetData() => data?.Clone();
		public static void SetData(ClockConfigData newData) => SetData(newData, out _);
		public static void SetData(ClockConfigData newData, out ClockConfigData prevData) {
			ThrowIfIsLocked();

			var okNewData = newData?.Clone();
			var tmpPrevData = Interlocked.Exchange(ref data, okNewData);
			prevData = tmpPrevData?.Clone();
		}
		public static void ResetData() => ResetData(out _);
		public static void ResetData(out ClockConfigData prevData) => SetData(null, out prevData);

		public static void SetUtcNow(Func<DateTimeOffset> utcNow) => SetUtcNow(utcNow, out _);
		public static void SetUtcNow(Func<DateTimeOffset> utcNow, out ClockConfigData prevData) => EditDataInternal(utcNow, ClockConfigData.WithUtcNow, out prevData);
		public static void SetLocalNow(Func<DateTimeOffset> localNow) => SetLocalNow(localNow, out _);
		public static void SetLocalNow(Func<DateTimeOffset> localNow, out ClockConfigData prevData) => EditDataInternal(localNow, ClockConfigData.WithLocalNow, out prevData);

		public static bool TrySetUtcNow(Func<DateTimeOffset> utcNow) => TrySetUtcNow(utcNow, out _);
		public static bool TrySetUtcNow(Func<DateTimeOffset> utcNow, out ClockConfigData prevData) => TrySetUtcNow(DefaultMaxAttempts, utcNow, out prevData);
		public static bool TrySetUtcNow(int maxAttempts, Func<DateTimeOffset> utcNow) => TrySetUtcNow(maxAttempts, utcNow, out _);
		public static bool TrySetUtcNow(int maxAttempts, Func<DateTimeOffset> utcNow, out ClockConfigData prevData) => TryEditDataInternal(maxAttempts, utcNow, ClockConfigData.WithUtcNow, out prevData);

		public static bool TrySetLocalNow(Func<DateTimeOffset> localNow) => TrySetLocalNow(localNow, out _);
		public static bool TrySetLocalNow(Func<DateTimeOffset> localNow, out ClockConfigData prevData) => TrySetLocalNow(DefaultMaxAttempts, localNow, out prevData);
		public static bool TrySetLocalNow(int maxAttempts, Func<DateTimeOffset> localNow) => TrySetLocalNow(maxAttempts, localNow, out _);
		public static bool TrySetLocalNow(int maxAttempts, Func<DateTimeOffset> localNow, out ClockConfigData prevData) => TryEditDataInternal(maxAttempts, localNow, ClockConfigData.WithLocalNow, out prevData);
	}
}
