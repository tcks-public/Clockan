﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clockan.Config
{
	public sealed class ClockConfigData {
		public readonly Func<DateTimeOffset> UtcNow;
		public readonly Func<DateTimeOffset> LocalNow;

		public ClockConfigData() { }
		public ClockConfigData(Func<DateTimeOffset> utcNow, Func<DateTimeOffset> localNow) {
			this.UtcNow = utcNow;
			this.LocalNow = localNow;
		}

		public ClockConfigData Clone() => Create(UtcNow, LocalNow);

		public static ClockConfigData Create(Func<DateTimeOffset> utcNow, Func<DateTimeOffset> localNow) => new ClockConfigData(utcNow, localNow);
		public static ClockConfigData Create(ClockConfigData original, Func<DateTimeOffset> utcNow, Func<DateTimeOffset> localNow) {
			return new ClockConfigData(utcNow ?? original?.UtcNow, localNow ?? original?.LocalNow);
		}

		public static ClockConfigData WithUtcNow(ClockConfigData original, Func<DateTimeOffset> utcNow) => new ClockConfigData(utcNow, original?.LocalNow);
		public static ClockConfigData WithLocalNow(ClockConfigData original, Func<DateTimeOffset> localNow) => new ClockConfigData(original?.UtcNow, localNow);
	}
}
