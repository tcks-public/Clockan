﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Clockan.Config {
	[Serializable]
	public class ConfigIsLockedException : ApplicationException {
		public ConfigIsLockedException() : base("Clock configuration is locked.") { }
		public ConfigIsLockedException(string message) : base(message) { }
		public ConfigIsLockedException(string message, Exception innerException) : base(message, innerException) { }
		protected ConfigIsLockedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
